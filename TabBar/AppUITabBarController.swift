//
//  AppUITabBarController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/3/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class AppUITabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let bannerView = reachability.instanceFromNib() as? reachability
        let connectedRef =
            Firebase.Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                bannerView!.removeFromSuperview()
                let window = UIApplication.shared.keyWindow!
                window.windowLevel = UIWindowLevelStatusBar - 1
                
            } else{
                
                    let window = UIApplication.shared.keyWindow!
                    window.addSubview(bannerView!)
                    window.windowLevel = UIWindowLevelNormal
                
            }
        })


    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
