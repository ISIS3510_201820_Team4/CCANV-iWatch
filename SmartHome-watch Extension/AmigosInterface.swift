//
//  AmigosInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/5/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation


class AmigosInterface: WKInterfaceController {
    
    var amigos = ["Juanita", "Javier", "Natalia", "Pedro"]
    
    
    @IBOutlet weak var tablaAmigos: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        self.tablaAmigos.setNumberOfRows(amigos.count, withRowType: "AmigosRow")
        
        for index in 0..<amigos.count{
            let row = self.tablaAmigos.rowController(at: index) as! AmigosRow
            
            row.AmigoLabel.setText(amigos[index])
            
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        
        self.pushController(withName: "AmigoInterface", context: amigos[rowIndex])
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
