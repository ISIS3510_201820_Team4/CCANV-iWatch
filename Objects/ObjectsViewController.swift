//
//  ObjectsViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/10/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import MBProgressHUD

class ObjectsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var objectInCurrentCircle = [CircleObject] ()
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        titleView.textColor = K.UI.main_color
        
        collectionView.register(UINib.init(nibName: "CircleObjectCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CircleObjectCollectionViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (K.Us.currentCircle != nil){
            MBProgressHUD.showAdded(to: self.view, animated: true)
           
            K.Us.currentCircle?.getObjects(callback: { (obj) in
                if(obj != nil){
                    if obj!.count > 0 {
                        self.noDataView.alpha = 0
                         self.noDataView.isHidden = true
                    } else{
                        self.noDataView.alpha = 1
                        self.noDataView.isHidden = false
                    }
                    self.objectInCurrentCircle = obj!
                    self.collectionView.reloadData()
                    MBProgressHUD.hide(for: self.view, animated: true)
                }else{
                     self.noDataView.alpha = 1
                    self.noDataView.isHidden = false
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            })
        }
    }

    override func viewWillLayoutSubviews() {
        addBtn.roundCorners(radius: addBtn.frame.size.height / 2)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectInCurrentCircle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CircleObjectCollectionViewCell", for: indexPath) as! CircleObjectCollectionViewCell
        cell.cicleObjectLabel.text = objectInCurrentCircle[indexPath.row].name
        cell.circleObjectImage.image = UIImage(named: "Obj3")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width / 2 * 0.9, height: self.collectionView.frame.height / 2 * 0.8 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ObjectVisualisationViewController") as! ObjectVisualisationViewController
        vc.objectSelected = objectInCurrentCircle[indexPath.row]
        present(vc, animated: true, completion: nil)
    }
    
    

}
