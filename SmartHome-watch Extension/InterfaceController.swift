//
//  InterfaceController.swift
//  iVision WatchKit Extension
//
//  Created by Andrés on 10/5/18.
//  Copyright © 2018 ACCAE. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    
    @IBOutlet weak var CirculosButton: WKInterfaceButton!
    
    @IBOutlet weak var AmigosButton: WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    
}
