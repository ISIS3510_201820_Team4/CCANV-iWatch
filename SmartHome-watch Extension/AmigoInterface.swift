//
//  AmigoInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/5/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation


class AmigoInterface: WKInterfaceController {
    
    
    @IBOutlet weak var nombreAmigoLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        let amigo = context as! String
        
        
        self.nombreAmigoLabel.setText(amigo)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
