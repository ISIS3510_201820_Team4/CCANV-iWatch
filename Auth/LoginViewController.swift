//
//  LoginViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/3/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import FirebaseAuth
import MBProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userField: UITextField!
    
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            loginBtn.roundCorners(radius: 20.5)
        hideKeyboardWhenTappedAround()
        passwordField.delegate = self
        userField.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews(){
       
    }
    
    @IBAction func login(_ sender: Any) {
        
        if passwordField.text != nil && passwordField.text?.isEmpty == false && userField.text != nil && userField.text?.isEmpty == false {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Auth.auth().signIn(withEmail: userField.text! , password: passwordField.text!) { (user, error) in
                
                if error != nil {
                    print("error: \(String(describing: error?.localizedDescription ))")
                     MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if error?.localizedDescription == "The password is invalid or the user does not have a password."
                    {
                        let alert = UIAlertController(title: "Error", message: "The password is invalid or the user does not have a password.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("error")
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }
                    if error?.localizedDescription == "There is no user record corresponding to this identifier. The user may have been deleted."{
                        let alert = UIAlertController(title: "Error", message: "There is no user record corresponding to this identifier. The user may have been deleted.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                  print("error")
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }else{
                    
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.dismiss(animated: true, completion: nil)
                   
                    
                }
                
            }
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
    
    
}
