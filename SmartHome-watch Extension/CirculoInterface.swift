//
//  CirculoInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/5/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation


class CirculoInterface: WKInterfaceController {
    
    var objetos = ["TV", "MAC", "iPhone"]
    
    @IBOutlet weak var nombreCirculoLabel: WKInterfaceLabel!
    
    @IBOutlet weak var TablaObjetos: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        
        super.awake(withContext: context)
        
        // Configure interface objects here.
        let circulo = context as! String
        
        self.nombreCirculoLabel.setText(circulo)
        
    self.TablaObjetos.setNumberOfRows(objetos.count, withRowType: "ObjetosRow")
        
        for index in 0..<objetos.count{
            let row = self.TablaObjetos.rowController(at: index) as! ObjetosRow
            
            row.ObjetoLabel.setTitle(objetos[index])
            
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
