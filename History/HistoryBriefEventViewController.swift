//
//  HistoryBriefEventViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/11/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class HistoryBriefEventViewController: UIViewController {

    @IBOutlet weak var iconImg: UIImageView!
    
    @IBOutlet weak var titleView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color

        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        iconImg.roundCorners(radius:iconImg.frame.size.width / 2 )
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func back(_ sender: Any) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
