//
//  CreateCircleViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/6/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MBProgressHUD

class CreateCircleViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    @IBOutlet weak var nameTxt: UITextField!
    
    @IBOutlet weak var imageViewCircle: UIImageView!
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var addCircleBtn: UIButton!
    @IBOutlet weak var addressLb: UILabel!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
     var locationManager: CLLocationManager!
     var initialGeoPoint: GeoPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         titleView.textColor = K.UI.main_color
        nameTxt.delegate = self
        addressTxt.delegate = self
        hideKeyboardWhenTappedAround()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startMonitoringSignificantLocationChanges()
    }
    override func viewDidLayoutSubviews() {
        addCircleBtn.roundCorners(radius: addCircleBtn.frame.height / 2)
        addCircleBtn.addCancelShadow()
        addCircleBtn.layer.shadowRadius = addCircleBtn.frame.size.height / 2
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        initialGeoPoint = GeoPoint(latitude: locValue.latitude, longitude: locValue.longitude)
        addressLb.text = "Using current location.."
        locationManager.stopUpdatingLocation()
        
    }

    
    @IBAction func createCircle(_ sender: Any)
    {
        if (nameTxt.text != "" && nameTxt.text != nil && initialGeoPoint != nil && addressTxt.text != "" && addressTxt.text != nil && imageViewCircle.image != nil ){
             MBProgressHUD.showAdded(to: self.view, animated: true)
            let date = Date()
            let filePath = "images/circles/" + date.description
            var data = NSData()
            data = UIImageJPEGRepresentation(imageViewCircle.image!, 0.8)! as NSData
            let meta = StorageMetadata()
            meta.contentType = "image/jpg"
            let ref = K.Database.storageRef().child(filePath)
                ref.putData(data as Data, metadata: meta){(metaData,error) in
                guard let metadata = metaData else {
                    // Uh-oh, an error occurred!
                    MBProgressHUD.hide(for: self.view, animated: true)
                    return
                }
                ref.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        MBProgressHUD.hide(for: self.view, animated: true)
                        return
                    }
                    
                    let cir = Circle([:])
                    cir.name = self.nameTxt.text
                    cir.leaderID = K.Us.user?.uid
                    cir.location = self.initialGeoPoint
                    cir.addrress = self.addressTxt.text
                    cir.imageUrl = url?.absoluteString
                    let par = UserParticipant([:])
                    par.name = K.Us.user?.name
                    par.phone = K.Us.user?.phone
                    if let ui = K.Us.user?.uid {
                        par.objectID = ui
                        par.ref =  K.FireStore.ref().collection("users").document(ui)
                    }
                    
                    cir.addParticipant(participant: par)
                    cir.save(route: "circles")
                    
                    if let us = K.Us.user {
                        let usC = UserCircle([:])
                        usC.name = self.nameTxt.text
                        if( cir.uid != nil){
                            usC.objectID = cir.uid
                            usC.ref = K.FireStore.ref().collection("circles").document(cir.uid!)
                            us.addCircle(circle:  usC)
                        }
                        
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let alert = UIAlertController(title: "Save", message: "Your circles was created.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            self.dismiss(animated: true, completion: nil)
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
        }
        
          
        }

    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func selectImage(_ sender: Any) {
        
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.imageViewCircle.image = image
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
