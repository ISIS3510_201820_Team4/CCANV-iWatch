//
//  NewParticipantViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class NewParticipantViewController: UIViewController {

    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var addQRbtn: UIButton!
    
    @IBOutlet weak var addEmailbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        navigationController?.navigationBar.isHidden = true

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        addQRbtn.roundCorners(radius: addQRbtn.frame.height / 2)
        addQRbtn.addCancelShadow()
        addQRbtn.layer.shadowRadius = addQRbtn.frame.size.height / 2
        addEmailbtn.roundCorners(radius: addEmailbtn.frame.height / 2)
        addEmailbtn.addCancelShadow()
        addEmailbtn.layer.shadowRadius = addEmailbtn.frame.size.height / 2
    }
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goAddQR(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeReaderViewController") as! QRCodeReaderViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func goAddEmail(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FindUserEmailViewController") as! FindUserEmailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
