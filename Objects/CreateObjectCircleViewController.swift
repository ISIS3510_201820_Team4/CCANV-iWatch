//
//  CreateObjectCircleViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/8/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

class CreateObjectCircleViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var objectImage: UIImageView!
    
    @IBOutlet weak var nameTxt: UITextField!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var scanBtn: UIButton!
    
    @IBOutlet weak var rfidLb: UILabel!
    
    @IBOutlet weak var titleView: UILabel!
    
    var rfidCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        hideKeyboardWhenTappedAround()
        navigationController?.navigationBar.isHidden = true
        nameTxt.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        objectImage.circleImage()
        addBtn.roundCorners(radius: addBtn.frame.size.height / 2)
        addBtn.addCancelShadow()
        addBtn.layer.shadowRadius = addBtn.frame.size.height / 2
        
        scanBtn.roundCorners(radius: scanBtn.frame.size.height / 2)
        scanBtn.addCancelShadow()
        scanBtn.layer.shadowRadius = scanBtn.frame.size.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if rfidCode != ""{
            rfidLb.text = rfidCode
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addObject(_ sender: Any) {
        
        if(nameTxt.text != nil && nameTxt.text != "" && objectImage.image != nil && rfidLb.text != "RFID Code"){
            MBProgressHUD.showAdded(to: self.view, animated: true)
                let date = Date()
                let filePath = "images/objects/" + date.description
                var data = NSData()
                data = UIImageJPEGRepresentation(self.objectImage.image!, 0.8)! as NSData
                let meta = StorageMetadata()
                meta.contentType = "image/jpg"
                let ref = K.Database.storageRef().child(filePath)
                ref.putData(data as Data, metadata: meta){(metaData,error) in
                    guard let metadata = metaData else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        // Uh-oh, an error occurred!
                        return
                    }
                    ref.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            MBProgressHUD.hide(for: self.view, animated: true)
                            return
                        }
                        let obj = CircleObject([:])
                        obj.name = self.nameTxt.text
                        obj.circleID = K.Us.currentCircle?.uid
                        obj.image = url?.absoluteString
                        obj.RFIDCode = self.rfidLb.text
                        obj.state = 0
                        K.Us.currentCircle?.addObject(object: obj)
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                        let alert = UIAlertController(title: "Save", message: "Your object was added.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                self.dismiss(animated: true, completion: nil)
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
        }
        
    }
    
    @IBAction func selectImage(_ sender: Any) {
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.objectImage.image = image
        }
    }
    
    
    @IBAction func scanCode(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeReaderViewController") as! QRCodeReaderViewController
        vc.from = "objects"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
