//
//  ObjectVisualisationViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 11/3/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class ObjectVisualisationViewController: UIViewController {

    weak var objectSelected : CircleObject?
    @IBOutlet weak var imageObject: UIImageView!
    
    @IBOutlet weak var objectName: UILabel!
    
    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var RFIDCode: UILabel!
    
    @IBOutlet weak var stateLb: UILabel!
    
    @IBOutlet weak var deteleBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if objectSelected != nil {
            objectName.text = objectSelected?.name
            RFIDCode.text = objectSelected?.RFIDCode
            
            if let sta = objectSelected?.state  {
                if sta == 1 {
                    stateLb.text = "Out of home"
                    stateLb.textColor = UIColor(netHex: 0xF96463)
                }else{
                    stateLb.text = "Inside the house"
                    stateLb.textColor = K.UI.main_color
                }
            }
            
            if let img = objectSelected?.image {
                imageObject.downloadedFrom(link: img )
            }
           
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        imageObject.circleImage()
        deteleBtn.roundCorners(radius: deteleBtn.frame.size.height / 2)
        deteleBtn.addCancelShadow()
        deteleBtn.layer.shadowRadius = deteleBtn.frame.size.height / 2
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteObject(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Save", message: "Really want to delete object?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                if self.objectSelected != nil {
                    K.Us.currentCircle?.deletObject(object: self.objectSelected!)
                }
                self.dismiss(animated: true, completion: nil)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
