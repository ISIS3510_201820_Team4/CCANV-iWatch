//
//  PermisosInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/5/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation


class PermisosInterface: WKInterfaceController {
    
    
    var circulos = ["Finca Anapoima", "Casa Eje Ambiental", "Apartamento Modelia"]
    
    @IBOutlet weak var permisosLabel: WKInterfaceLabel!
    
    
    @IBOutlet weak var TablaCirculos: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        self.TablaCirculos.setNumberOfRows(circulos.count, withRowType: "CirculosPermisosRow")
        
        for index in 0..<circulos.count{
            let row = self.TablaCirculos.rowController(at: index) as! CirculosPermisosRow
            
           row.CirculoSwitch.setTitle(circulos[index])
    
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "CirculoInterface", context: circulos[rowIndex])
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
