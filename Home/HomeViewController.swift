//
//  HomeViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/10/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import FirebaseAuth
import MBProgressHUD
import Firebase

class HomeViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate{
    
    
    
    @IBOutlet weak var iconView: UIView!
    
    @IBOutlet weak var unlockBtn: UIButton!
    
    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var placeTextField: UITextField!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var emmergencyBtn: UIButton!
    
    var places = [UserCircle] ()
    
    weak var currentCircle :UserCircle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        iconView.backgroundColor = K.UI.main_color
        unlockBtn.backgroundColor = K.UI.main_color
        // Do any additional setup after loading the view.
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.white
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancel))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        placeTextField.inputAccessoryView = toolBar
        
        placeTextField.inputView = pickerView
        
        
        
        K.Us.starListener()
        Auth.auth().addStateDidChangeListener(){auth, user in
            if(K.Us.listener){
                MBProgressHUD.showAdded(to: self.view, animated: true)
                
                if user != nil {
                    
                    User.withID(id: (user?.uid)! , callback: { (user) in
                        
                        if let p = user {
                            
                            K.Us.user = p
                            
                            if let token = Firebase.Messaging.messaging().fcmToken {
                                print("new token  \( token)")
                                K.Us.user!.saveNotificationToken(token: token)
                                let defaults = UserDefaults.standard
                                defaults.set(token, forKey: "deviceToken")
                            }
                            if let dat = K.Us.user?.getCircles(){
                                self.places = dat
                                self.placeTextField.text = dat.first?.name
                                if let rf = dat.first?.objectID {
                                    Circle.WithRef(ref: K.FireStore.ref().collection("circles").document(rf)) { (cir) in
                                        K.Us.currentCircle = cir
                                        print("llego aca !!!!!))))))))))))) 5")
                                        
                                    }
                                }
                                
                                
                                
                            }
 
                        }
                        
                         MBProgressHUD.hide(for: self.view, animated: true)
                    })
                    
                    
                    
                    
                }
                else {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    K.Us.closeListener()
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(vc, animated: true, completion: nil)
                    
                }
            }
        }
        
        
        
    }
    
    
    override func viewWillLayoutSubviews() {
        
        addBtn.roundCorners(radius: addBtn.frame.size.height / 2)
        iconView.roundCorners(radius:iconView.frame.size.width / 2 )
        unlockBtn.roundCorners(radius: unlockBtn.frame.size.height / 2)
        unlockBtn.addCancelShadow()
        unlockBtn.layer.shadowRadius = unlockBtn.frame.size.height / 2
        emmergencyBtn.roundCorners(radius: emmergencyBtn.frame.size.height / 2)
        emmergencyBtn.addCancelShadow()
        emmergencyBtn.layer.shadowRadius = emmergencyBtn.frame.size.height / 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if  (Auth.auth().currentUser != nil){
            
            if let uid = Auth.auth().currentUser?.uid {
                User.withID(id: uid) { (us) in
                    K.Us.user = us
                    if let dat = K.Us.user?.getCircles(){
                        self.places = dat
                    }
                    
                }
            }
            
        }
    }
    
    
    
    
    // This function sets the text of the picker view to the content of the "salutations" array
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return places[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        placeTextField.text = places[row].name
        currentCircle = places[row]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return places.count
    }
    
    
    @IBAction func createCircle(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateCircleViewController") as! CreateCircleViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @objc func donePicker(){
        placeTextField.resignFirstResponder()
        
        if let rf = currentCircle {
            if let r = rf.objectID {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Circle.WithRef(ref: K.FireStore.ref().collection("circles").document(r)) { (cir) in
                    K.Us.currentCircle = cir
                    print("llego aca !!!!!))))))))))))) 6")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.currentCircle = nil
                    
                }
                
            }
            
        }
    }
    
    @objc func cancel(){
        placeTextField.resignFirstResponder()
        
    }
    
    
    
    @IBAction func showQR(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func declareEmergency(_ sender: Any) {
        
        try! Auth.auth().signOut()
        
    }
    
    
}
