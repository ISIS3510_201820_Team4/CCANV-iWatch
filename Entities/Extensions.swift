//
//  Extensions.swift
//  DocDocPatient
//
//  Created by juan esteban chaparro on 21/10/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import MBProgressHUD
import GoogleMaps

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension UIView {
    
    func mainshadow( color: UIColor){
        
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
    }
    
    func addNormalShadow() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func addShadowChat() {

        self.layer.shadowColor = UIColor(netHex: 0x9B9B9B).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3.0

    }
    
    func addShadowModal() {
        
        self.layer.shadowColor = UIColor(netHex: 0x000000).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 4.0
        
    }
    
    func addShadowChatProfile() {
        
        self.layer.shadowColor = K.UI.shadow_color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 3.0
        
    }
    
    func consultationShadow() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func addCancelShadow() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 12.0)
        self.layer.shadowOpacity = 0.24
        self.layer.shadowPath = shadowPath.cgPath
    }
    func deleteShadow() {
        self.layer.shadowOpacity = 0.0
    }
    
    func addInvertedShadow() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: -2.0)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func addSpecialShadow(size: CGSize) {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = 0.15
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func addHomeCellShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.08
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        
    }
    func addInventoryShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
    func addLightShadow() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.05
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func clearShadows() {
        self.layer.shadowOpacity = 0.0
    }
    
    func roundCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func bordered(color:UIColor) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func addInnerShadow() {
        self.layer.borderColor = UIColor(netHex:0x545454).withAlphaComponent(0.3).cgColor
        self.layer.borderWidth = 1.0
    }
    
    func addGradientBackground(_ first: UIColor, _ second: UIColor, start: NSNumber = 0.0, end: NSNumber = 1.0, horizontal: Bool = false, diagonal: Bool = false) {
        let colorTop =  first.cgColor
        let colorBottom = second.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [start, end]
        gradientLayer.frame = self.bounds
        
        if horizontal {
            gradientLayer.startPoint = diagonal ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonal ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonal ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonal ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    

    
    func setGradientBackground(_ first: UIColor, _ second: UIColor, start: NSNumber = 0.0, end: NSNumber = 1.0, horizontal: Bool = false, diagonal: Bool = false) -> CALayer {
        let colorTop =  first.cgColor
        let colorBottom = second.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [start, end]
        gradientLayer.frame = self.bounds
        
        if horizontal {
            gradientLayer.startPoint = diagonal ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonal ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonal ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonal ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
        
        self.layer.insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func playTapped() {
        
        if let viewWithTag = self.view.viewWithTag(999999) {
            viewWithTag.removeFromSuperview()
            //            darkenView.alpha = 0
        }
    }
}

extension CAGradientLayer {
    
    func roundCorners(radius: CGFloat) {
        self.cornerRadius = radius
        self.shadowRadius = radius
    }
}
extension UIViewController {
    
    func showAlert(title:String, message:String, closeButtonTitle:String) {
        let alertController = UIAlertController(title: title, message: message,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: closeButtonTitle, style: .default) { (action: UIAlertAction) in
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true) { }
    }
    
    func setUpSmartKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisplay(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clearKeyboards)))
    }
    
    @objc func keyboardWillDisplay(notification:NSNotification) {
        let userInfo:Dictionary = notification.userInfo!
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x: 0.0, y: ((self.originalFrame().origin.y)-keyboardHeight)*self.needsDisplacement(), width: (self.originalFrame().size.width), height: (self.originalFrame().size.height))
        })
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = self.originalFrame()
        })
    }
    
    func needsDisplacement() -> CGFloat {
        return CGFloat(0)
    }
    
    func originalFrame() -> CGRect {
        return CGRect.zero
    }
    
    func keyboards() -> [UITextField] {
        return []
    }
    
    @objc func clearKeyboards(index: Int = -1) {
        let kb = keyboards()
        for k in kb {
            k.resignFirstResponder()
        }
        if (index > -1 && index < kb.count) {
            kb[index].becomeFirstResponder()
        }
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        let mb = MBProgressHUD.showAdded(to: self, animated: true)
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                mb.hide(animated: true)
                self.image = image
//                if(K.User.profile){
//                K.User.patient?.image = image
//                    K.User.profile = false
//                }
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    func circleImage() {
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true
    }
}

extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
// Logical

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
    
    func empty() -> Bool {
        return self.count == 0
    }
    
    var head: Element? {
        get {
            return self.first
        }
    }
    
    var tail: Array<Element>? {
        get {
            if self.empty() { return nil }
            return Array(self.dropFirst())
        }
    }
    
    func foldl<A>(acc: A, list: Array<Element>,f: (A, Element) -> A) -> A {
        if list.empty() { return acc }
        return foldl(acc: f(acc, list.head!), list: list.tail!, f: f)
    }
    
    subscript(indexes: [Int]) ->  [Element] {
        var result: [Element] = []
        for index in indexes {
            if index > 0 && index < self.count {
                result.append(self[index])
            }
        }
        return result
    }
}

extension Date {
    
    enum DateFormat {
        case Short
        case Default
        case Medium
        case Long
        case Time
        case Try
        case Custom(String)
    }
    
    func merge(time: Date) -> Date? {
        let calendar = Calendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let timeComponents = calendar.dateComponents([.hour, .minute], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = dateComponents.day!
        mergedComponments.hour = timeComponents.hour!
        mergedComponments.minute = timeComponents.minute!
        
        return calendar.date(from: mergedComponments)
    }
    
    init?(fromString: String, withFormat: DateFormat) {
        let dtf = DateFormatter()
        switch withFormat {
        case .Short:
            dtf.dateFormat = K.Helper.fb_date_short_format
        case .Default:
            dtf.dateFormat = K.Helper.fb_date_format
        case .Medium:
            dtf.dateFormat = K.Helper.fb_date_medium_format
        case .Long:
            dtf.dateFormat = K.Helper.fb_long_date_format
        case .Time:
            dtf.dateFormat = K.Helper.fb_time_format
        case .Try:
            dtf.dateFormat = K.Helper.fb_date_format
            if let tst_dt = dtf.date(from: fromString) {
                self = tst_dt
                return
            }
            dtf.dateFormat = K.Helper.fb_date_short_format
            if let tst_dt = dtf.date(from: fromString) {
                self = tst_dt
                return
            }
            dtf.dateFormat = K.Helper.fb_date_medium_format
            if let tst_dt = dtf.date(from: fromString) {
                self = tst_dt
                return
            }
            dtf.dateFormat = K.Helper.fb_long_date_format
            if let tst_dt = dtf.date(from: fromString) {
                self = tst_dt
                return
            }
            dtf.dateFormat = K.Helper.fb_time_format
            if let tst_dt = dtf.date(from: fromString) {
                self = tst_dt
                return
            }
            else {
                return nil
            }
        case .Custom(let format):
            dtf.dateFormat = format
        }
        
        self = dtf.date(from: fromString)!
    }
    
    func toString(format: DateFormat) -> String? {
        let dtf = DateFormatter()
        switch format {
        case .Short:
            dtf.dateFormat = K.Helper.fb_date_short_format
        case .Default, .Try:
            dtf.dateFormat = K.Helper.fb_date_format
        case .Medium:
            dtf.dateFormat = K.Helper.fb_date_medium_format
        case .Long:
            dtf.dateFormat = K.Helper.fb_long_date_format
        case .Time:
            dtf.dateFormat = K.Helper.fb_time_format
        case .Custom(let format):
            dtf.dateFormat = format
            let str = dtf.string(from: self)
            if str != "" {
                return str
            }
        }
        let str = dtf.string(from: self)
        return str
    }
    
    func bisiesto() -> Bool {
        
        let calendar = Calendar.current
        let yearTemp = calendar.component(.year, from: self)
        
        if (Int(yearTemp) % 100 == 0 && Int(yearTemp) % 400 == 0 ){
            return true
        }
        
        
        return false
    }
    
    func toCalendar() -> String? {
        
        var month = ""
        
        let calendar = Calendar.current
        
        let monthTemp = calendar.component(.month, from: self)
        
        let dayTemp = calendar.component(.day, from: self)
        
        let yearTemp = calendar.component(.year, from: self)
        
        
        if monthTemp == 1{
            month = "Enero"
        }
        if monthTemp == 2{
            month = "Febrero"
        }
        if monthTemp == 3{
            month = "Marzo"
        }
        if monthTemp == 4{
            month = "Abril"
        }
        if monthTemp == 5{
            month = "Mayo"
        }
        if monthTemp == 6{
            month = "Junio"
        }
        if monthTemp == 7{
            month = "Julio"
        }
        if monthTemp == 8{
            month = "Agosto"
        }
        if monthTemp == 9{
            month = "Septiembre"
        }
        if monthTemp == 10{
            month = "Octubre"
        }
        if monthTemp == 11{
            month = "Noviembre"
        }
        if monthTemp == 12{
            month = "Diciembre"
        }
        
        
        return "  " + String(dayTemp) + "      " + month + "      " + String(yearTemp)
    }
    
    func getMontEn() -> String {
        
        var month = ""
        
        let calendar = Calendar.current
        
        let monthTemp = calendar.component(.month, from: self)
        
        if monthTemp == 1{
            month = "ENE"
        }
        if monthTemp == 2{
            month = "FEB"
        }
        if monthTemp == 3{
            month = "MAR"
        }
        if monthTemp == 4{
            month = "ABR"
        }
        if monthTemp == 5{
            month = "MAY"
        }
        if monthTemp == 6{
            month = "JUN"
        }
        if monthTemp == 7{
            month = "JUL"
        }
        if monthTemp == 8{
            month = "AGO"
        }
        if monthTemp == 9{
            month = "SEP"
        }
        if monthTemp == 10{
            month = "OCT"
        }
        if monthTemp == 11{
            month = "NOV"
        }
        if monthTemp == 12{
            month = "DIC"
        }
        
        
        return month
    }
    
}




extension String {

        func condenseWhitespace() -> String {
            let components = self.components(separatedBy: .whitespacesAndNewlines)
            return components.filter { !$0.isEmpty }.joined(separator: " ")
        }
    mutating func insert(separator: String, every n: Int) {
        self = inserting(separator: separator, every: n)
    }
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self.characters)
        stride(from: 0, to: characters.count, by: n).forEach {
            result += String(characters[$0..<min($0+n, characters.count)])
            if $0+n < characters.count {
                result += separator
            }
        }
        return result
    }
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
//    subscript (r: Range<Int>) -> String {
//        let start = index(startIndex, offsetBy: r.lowerBound)
//        let end = index(startIndex, offsetBy: r.upperBound)
//        return String(self[Range(start ..< end)])
//    }
}

extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}


extension NSDate {
    
    
    func getHour() -> Int? {
        
        // *** create calendar object ***
        var calendar = NSCalendar.current
        
        // *** Get components using current Local & Timezone ***
        print(calendar.dateComponents([.year, .month, .day, .hour, .minute], from: self as Date))
        
        // *** define calendar components to use as well Timezone to UTC ***
        let unitFlags = Set<Calendar.Component>([.hour, .year, .minute])
        calendar.timeZone = TimeZone(identifier: "UTC")!
        
        // *** Get All components from date ***
        let components = calendar.dateComponents(unitFlags, from: self as Date)
        print("All Components : \(components)")
        return calendar.component(.hour, from: self as Date)
    }
    func toCalendar() -> String? {
        
        var month = ""
        
        let calendar = Calendar.current
        
        let monthTemp = calendar.component(.month, from: self as Date)
        
        let dayTemp = calendar.component(.day, from: self as Date)
        
        let yearTemp = calendar.component(.year, from: self as Date)
        
        
        if monthTemp == 1{
            month = "Enero"
        }
        if monthTemp == 2{
            month = "Febrero"
        }
        if monthTemp == 3{
            month = "Marzo"
        }
        if monthTemp == 4{
            month = "Abril"
        }
        if monthTemp == 5{
            month = "Mayo"
        }
        if monthTemp == 6{
            month = "Junio"
        }
        if monthTemp == 7{
            month = "Julio"
        }
        if monthTemp == 8{
            month = "Agosto"
        }
        if monthTemp == 9{
            month = "Septiembre"
        }
        if monthTemp == 10{
            month = "Octubre"
        }
        if monthTemp == 11{
            month = "Noviembre"
        }
        if monthTemp == 12{
            month = "Diciembre"
        }
        
        
        return "  " + String(dayTemp) + "      " + month + "      " + String(yearTemp)
    }
    
    
    
    
}







